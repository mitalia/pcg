    org 100h

section .text

start:
    mov si,data
    mov dl,'A'
next:
    mov cl,dl
    and cl,63
    ;sub cx,'A'-1
    mov ah,2
r:
    int 21h
    loop r
    ;mov dl,10
    ;int 21h
    lodsb
    add dl,al
    jnz next
    ret
data:
    db 'W'-'A'
    db 'S'-'W'
    db 'A'-'S'
    db 'L'-'A'
    db 'I'-'L'
    db 'L'-'I'
    db 'A'-'L'
    db 'N'-'A'
    db 'D'-'N'
    db   0-'D'
    ;db "WSALILAND",0
