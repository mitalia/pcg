class Trie:
    def __init__(self):
        self.children={}
        self.payload=None
        self.progeny=0

    def getTerminal(self, word, create, increment=0):
        if len(word) == 0:
            return self
        w0=word[0]
        if w0 not in self.children:
            if create:
                self.children[w0]=Trie()
            else:
                return None

        self.progeny+=increment
        return self.children[w0].getTerminal(word[1:], create, increment)

    def addWord(self, word, payload=None):
        p = self.getTerminal(word, True, 1)
        if p.payload:
            raise KeyError('Payload already present')
        p.payload=payload

    def dump(self, prefix=''):
        if self.payload:
            print prefix, " > ", self.payload
        for k,v in self.children.iteritems():
            if len(self.children)==1:
                k='.'
            v.dump(prefix+k)

    def firstPayload(self):
        if self.payload is not None:
            return self.payload
        return self.children.values()[0].firstPayload()

    def minimize(self):
        if self.progeny==1:
            self.payload=self.firstPayload()
            self.children={}
        else:
            for v in self.children.itervalues():
                v.minimize()

    def __repr__(self):
        v=(self.children, )
        if self.payload:
            v=v+(self.payload, )
        return repr(v)


class TrieElem:
    def __init__(self, level, letter):
        self.level=level
        self.letter=letter

    def __repr__(self):
        return repr(self.__dict__)

class Payload:
    def __init__(self, doubleFirst, pos1, pos2, extras):
        self.doubleFirst=doubleFirst
        if pos2 is None:
            pos2 = 6
        self.pos1=(pos1+8)%8
        self.pos2=(pos2+8)%8
        self.extras=extras

    def __repr__(self):
        return repr(self.__dict__)

def trieToIR(trie):
    ir=[]

    def helper(trie, level):
        if trie.payload:
            ir.append(trie.payload)
        for k, v in trie.children.iteritems():
            if len(trie.children)!=1:
                ir.append(TrieElem(level+1, k))
            helper(v, level+1)

    helper(trie, 0)
    return ir

def IRToByteStream(IR):
    bs=""
    lastTE=TrieElem(0, '')
    for el in IR:
        if isinstance(el, TrieElem):
            b=0
            delta=el.level-lastTE.level
            if delta!=0:
                b|=1<<5
                if delta<0:
                    b|=1<<6
                    delta=-delta
                if delta>1:
                    b|=delta-2+27
                    bs+=chr(b)
                    b=0
            b|=ord(el.letter)-65
            bs+=chr(b)
            lastTE=el
        elif isinstance(el, Payload):
            b=1<<7
            if el.doubleFirst:
                b|=1<<6
            b|=el.pos1<<3
            b|=el.pos2<<3
            bs+=chr(b)
            bs+=''.join([chr(ord(c)-65) for c in el.extras])
        else:
            print "Unexpected element in IR:", repr(el)
    return bs

f = open("input.txt", "r")
m={}
t=[]
m2={}
trie=Trie()
for r in f:
    el,name=r.split()
    el,name = el.upper(),name.upper()
    w = []
    extras = []
    for l in el:
        ff=name.find(l)
        w.append(ff)
        if ff==-1:
            extras.append(l)
    dup=len(w)==3
    if dup:
        w=w[1:]
    if len(w)==1:
        w.append(None)
    trie.addWord(name, Payload(dup, w[0], w[1], extras))

print "Trie:"
trie.dump()
trie.minimize()
print "Trie after minimization:"
trie.dump()
print repr(trie)
IR=trieToIR(trie)
bs=IRToByteStream(IR)
print len(IR)
print len(bs)
print repr(bs)
