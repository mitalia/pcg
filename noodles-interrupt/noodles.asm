    org 100h

section .text

%define counter lop+1   ; see below
start:
    ; setup interrupt handler
    mov ax,251ch                ; function 25h (replace interrupt vector)
                                ; interrupt 1ch (user timer)
    mov dx,interrupt_handler    ; timer ISR
    int 21h
lop:
    mov cx,910  ; the 910 immediate value is actually pointed by counter,
                ; which is decremented in interrupt_handler
    loop lop    ; decrement and loop as long as cx is nonzero (the decrement is
                ; not relevant, we are always resetting cx at each iteration)
    mov ah,2    ; function 2 (print character); dl is already a printable character
    int 21h
    ret         ; quit

interrupt_handler:
    dec word[counter]   ; at every tick decrement the counter
    iret
