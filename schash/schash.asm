bits 16
org 100h

start:
    xor dx,dx       ; reset dx (used as accumulator)
readch:
    mov ah,1
    int 21h         ; read character
    jnc semicolon
    ret             ; quit if EOF
semicolon:
    cmp al,';'      ; is it a semicolon?
    jne hash        ; if not, skip to next check
    inc dx          ; increment accumulator
    cmp dl,127      ; if we get to 127, reset it; this saves us the
    je start        ; hassle to perform the modulo when handling #
hash:
    cmp al,'#'      ; is it a hash?
    jne readch      ; if not, skip back to character read
    mov ah,2        ; print dl (it was choosen as accumulator exactly
    int 21h         ; because it's the easiest register to print)
    jmp start       ; reset the accumulator and go on reading
