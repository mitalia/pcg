#!/usr/bin/env python
# encoding used:
#+-------+-------+---------------------+
#| Bit 7 | Bit 6 | Bit 5-0 meaning     |
#|   0   |  bits of "normal" character |
#|   1   |   0   |   count of spaces   |
#|   1   |   1   |   count of carets   |
#+-------+-------+---------------------+

import sys
s=None
o=sys.stdout

# read the input
with open(sys.argv[1]) as f:
    s=f.read()

magic=' ^'
# previous character
prev=""
# current sequence count
count=1
# guarantees us a different character as last
s+="\0"
for c in s:
    # increment until we can fit the counter in the lower 7 bits of a byte
    if c==prev and count<64 and (c in magic):
        count+=1
    else:
        # if we had a sequence, encode it
        if count>1:
            o.write(chr(128 | count | (magic.find(prev)<<6)))
            count = 1
        else:
            o.write(prev)
    prev=c
