    org 100h

start:
    ; si: pointer to current position in data
    mov si,data
    ; load the character in al
    lodsb
mainloop:
    ; bx: repetition count
    ; - zero at startup
    ; - -1 after each RLE run
    ; - one less than each iteration after each "literal" run
    ; the constant decrement is not really a problem, as print
    ; always does at least one print, and there aren't enough
    ; consecutive literal values to have wraparound

    ; if the high bit is not set, we have a "literal" byte;
    ; we prepare it in dl just in case
    mov dl,al
    ; then check if it's not set and branch straight to print
    ; notice that bx=0 is fine, as print prints always at least one character
    ; test the top two bits (we need the 6th bit below)
    test al,0xc0
    ; to see if the top bit was set, we interpret it as the sign bit,
    ; and branch if the number is positive or zero (top bit not set)
    jge print
rle:
    ; it wasn't a literal, but a caret/space with a repetition count
    ; space if 6th bit not set, caret otherwise
    mov dl,' '
    ; exploit the parity bit to see if the 6th bit was set
    jnp nocaret
    mov dl,'^'
nocaret:
    ; lower 6 bits: repetition count
    ; and away the top bits and move in bx
    ; we and ax and not al because we have to get rid of the 02h in ah
    and ax,3fh
    xchg ax,bx
print:
    ; print bx times
    mov ah,2
    int 21h
    dec bx
    jg print
    ; read next character
    lodsb
    test al,al
    ; rinse & repeat unless we got a zero
    jnz mainloop
end:
    ret
data:
    ; here be data
    incbin "compressed.dat"
    ; NUL terminator
    db 0
