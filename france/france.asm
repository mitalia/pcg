    ; usual .COM boilerplate
    org 100h

section .text

start:
    ; point the ES segment to the graphics memory
    push 0x0b800
    pop es
    ; bx is used (mainly) as repository for the background colors
    ; 1 => blue; f => white; 4 => red; 0 => flag for new row
    mov bx,0x1f40
    ; reset the destination pointer
    xor di,di
newrow:
    ; === start of a new row ===
    ; round down DI to multiples of 16, to avoid overflowing the row by one
    ; character (27*3 = 81, while the row is 80 characters)
    and di,0xfff0
span:
    ; === start of a span ===
    ; build the pattern to write; start with BX
    mov ax,bx
    ; adjust the pattern in AX:
    ; - keep the upper byte (background color);
    ; - leave blue as foreground color at the very end (avoid the prompt
    ;   ruining the flag when we exit);
    ; - put NUL as character to display;
    and ax,0xf100
    ; rotate bx, to switch to the next color pattern
    rol bx,4
    ; check if we got to the sentinel value (=> we finished one row)
    test ah,0xf0
    ; in this case, go back to newrow; this will re-align DI and do a new
    ; rotation on BX
    jz newrow
    ; fill the video memory with 27 times the word built above in AX
    mov cl,27
    rep stosw
    ; continue as far as DI is positive; notice that this way we keep writing
    ; well past the 25 lines limit, but doesn't seem to impact anything
    test di,di
    jg span
    ; otherwise exit; the point of this thing is to avoid a wraparound, which
    ; ends up unaligned, thus breaking the drawing
    ret
