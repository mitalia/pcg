    org 100h

section .text

start:
    ; bubble sort - external loop
ext:
    ; start from the first character (assume bh=0, true on every DOS)
    mov bl,82h
    ; "not-swapped" flag - 82h => no swaps in current iteration;
    ; 0 => a swap happened (the 82h will come in handy later)
    mov dx,bx
    ; bubble sort - internal loop
int:
    ; read 2 characters at time in the full ax
    mov ax,word[bx]   ; al     ah
                      ; ^[bx]  ^[bx+1]
    ; check if we are at the end (the command line is CR terminated)
    cmp ah,0dh
    je skip
    ; make uppercase in cx
    mov cx,2020h
    or cx,ax
    ; compare
    cmp cl,ch
    jle next
    ; wrong order - swap and rewrite
    xchg al,ah
    mov word[bx],ax
    ; mark that we did a swap
    xor dx,dx
next:
    ; next character
    inc bx
    jmp int
skip:
    ; loop as far as we swapped something
    test dx,dx
    jz ext
end:
    ; $-terminate the string
    mov byte[bx+1],'$'
    ; print
    ; dx already contains the location of the string, since that's the
    ; flag value we used for "no swaps"
    mov ah,9
    int 21h
    ret
