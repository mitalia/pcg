    org 100h

section .text

start:
    mov ah,4
    int 1ah             ; get the date from BIOS; cx now contains the year in packed BCD
    mov di,placeholder  ; put di on the last character of placeholder
lop:
    mov al,cl
    and al,0xf  ; get the low nibble of cx
    add [di],al ; add it to the digit
    dec di      ; previous character
    shr cx,4    ; next nibble
    jnz lop     ; loop as long as we have digits to unpack in cx
    mov dx,its
    mov ah,9
    int 21h     ; print the whole string
    ret

its:
    db "It's 000"
placeholder:
    db "0 already, folks, go home.$"
