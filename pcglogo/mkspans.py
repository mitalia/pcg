#!/usr/bin/env python2
import sys
from PIL import Image
import numpy as np

# transforms an array of bichromatic pixel values to a list of
# span lengths
# span lengths are limited by bits_per_span
def to_spans(img, bits_per_span):
    # calculate maximum span length
    max_span = (1<<bits_per_span)-1
    # assume a span starting with 1
    l = 1
    # current span length count
    count = 0
    # output list
    out = []
    for v in img:
        if v!=l:
            # we changed color - append the span length
            l=v
            # if the length is over the limit, split it into several
            # spans (adding a zero-length one for the other color)
            while count>max_span:
                count-=max_span
                out+=[max_span, 0]
            out+=[count]
            count=0
        count+=1
    out+=[count]
    return out

# Old test try various bits limits for the spans, and output the total
# bytes length
def samples_test(img):
    for i in range(1,9):
        out = to_spans(img, i)
    print i, (len(out)*i+7)/8

# read the image
img = np.array(Image.open(sys.argv[1])).flatten()
# convert to half-byte spans
l = to_spans(img, 4)
# let's guarantee we have an even number of spans
if len(l) & 1:
    # dirty trick: make the last span 1 pixel long even if it would
    # be zero; this allows us to save on check for zero-length cyan
    # spans in the assembly code, and at the end an extra pixel
    # doesn't matter anyway
    l+=[1]
# encode in bytes (upper 4 bits: white span; lower: cyan span)
sys.stdout.write(
    ''.join(chr((l[i*2]<<4) | l[i*2+1])
                for i in range(len(l)/2)))
