    org 100h

start:
    mov bx,spans        ; bx: pointer to the spans data
    mov di,headerend    ; we append to the header (at the end of the .COM)
    dec ax              ; ax=-1 (assume ax=0 at start, true for every DOS)
lop:
    mov cl,[bx]         ; read the next spans byte
    shr cx,4            ; extract the top 4 bits (length of white span)
    jz skip1            ; ignore if it's 0 pixels long
l1:
    stosb               ; write ff
    stosw               ; write ffff
    loop l1             ; repeat for the length of the span
skip1:
    mov cl,[bx]         ; re-read the spans byte
    and cl,0xf          ; take the low 4 bits (length of the cyan span)
l2:
    mov si,cyan         ; copy the 3 bytes starting at label cyan
    movsb               ; (i.e. it's RGB value)
    movsw
    loop l2             ; repeat for the length of the span
    inc bx              ; next span byte
    mov dx,header       ; check if we finished the spans (they end at the
    cmp bx,dx           ; start of the header)
    jl lop              ; if not, loop
    mov ax,0x0924       ; load al with '$' (for stosb), ah with 9 (for int 21h)
    stosb               ; append $ to terminate the string
    int 21h             ; write the string to standard output
    ret                 ; quit

cyan:
    db 0x64,0xb0,0xdf   ; RGB for the PCG cyan

spans:
    incbin "spans.dat"  ; include the spans data
header:
    db "P6 16 16 255 "  ; PPM header
headerend:              ; here at runtime we'll append the image data
