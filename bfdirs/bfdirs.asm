BITS 64
global _start

_start:
    ; pos
    xor eax,eax
    ; l
    mov ecx,1
    ; r
    mov edx,1
    ; cur char
    xor bl,bl
    mov rsi,[rsp+16]
    dec rsi
loop:
    inc rsi
    mov bl,[rsi]
    test bl,bl
    jz out
    cmp bl,'<'
    jl rarrow
    jg greater
    cmp [rsi+1],byte '-'
    je larrow
less:
    dec eax
    inc ecx
    jmp loop
greater:
    inc eax
    inc edx
    jmp loop
larrow:
    inc rsi
    sub eax,ecx
    mov ecx,1
    jmp loop
rarrow:
    inc rsi
    add eax,edx
    mov edx,1
    jmp loop
out:
    xor rsi,rsi
    mov rdi,rsp
    mov ecx,10
    dec rsp
    mov [rsp],byte 10
    test eax,eax
    jge digit
    neg eax
    inc rsi
digit:
    xor edx,edx
    div ecx
    dec rsp
    mov [rsp],dl
    add byte[rsp],'0'
    test eax,eax
    jnz digit

    test rsi,rsi
    jz nosign
    dec rsp
    mov [rsp],byte '-'
nosign:
    mov rdx,rdi
    mov rdi,1
    sub rdx,rsp
    mov rax,1
    mov rsi,rsp
    syscall
    mov rax,60
    syscall
