import sys
import re
r = re.compile(r"^[0-9a-f]{8} *([0-9a-f]+) *(.*)$", re.I)
code = 0
tot = 0
first = True
print "Gate    Hex      Assembly"
print "====    ====     ========"
for l in sys.stdin:
    m = r.match(l.strip())
    if m is None:
        print "---- error!", l
    by = m.group(1)
    asm = m.group(2)
    if first:
        print bin(code)[2:].rjust(4, '0'), "  ",
    if asm.lower().strip() == "nop":
        code+=1
        if first:
            print "         (nothing to do)"
        print
        first = True
        continue
    tot += len(by)/2
    if not first:
        print '       ',
    print by.ljust(8), asm
    first = False

print "Total:",tot
