
BITS 16

l0000:  ; false
    xor ax,ax
    nop
l0001:  ; and
    and ax,bx
    nop
l0010:
    not bx
    and ax,bx
    nop
l0011:  ; A
    nop
l0100:
    not ax
    and ax,bx
    nop
l0101:  ; B
    mov ax,bx
    nop
l0110:  ; xor
    xor ax,bx
    nop
l0111:  ; or
    or ax,bx
    nop
l1000:  ; nor
    or ax,bx
    not ax
    nop
l1001:  ; xnor
    mov ax,1
    nop
l1010:  ; not B
    not bx
    mov ax,bx
    nop
l1011:  ; if B then A
    not bx
    or ax,bx
    nop
l1100:  ; not A
    not ax
    nop
l1101:  ; if A then B
    not ax
    or ax,bx
    nop
l1110:  ; nand
    and ax,bx
    not ax
    nop
l1111:  ; true
    xor ax,ax
    not ax
    nop
