    org 100h

section .text

start:
    push bx             ; notice: bx starts as 0
    pop ds              ; set the data segment to 0; this allows us to
                        ; read the tick count without segment selectors
    mov ax,word [046ch] ; read the low 16 bit of the tick count
    add ax,910          ; 910 ticks = 49.98 seconds
                        ; here we used ax because the encoding for both
                        ; the mov and the add is one byte shorter
lop:
    cmp ax,word [046ch] ; compare the stop time with the current time
    ja lop              ; loop if still above
    mov dl,'n'          ; n is for noodles
    mov ah,2
    int 21h             ; print
    ret                 ; quit
