    org 100h

section .text

start:
    mov ah,8h       ; ah starts at 08h (read console, no echo)
    mov dl,'*'      ; write asterisks (we could have left whatever
                    ; startup value we have here, but given that dx=cs,
                    ; we have no guarantee to get a non-zero non-space
                    ; value)
lop:
    ; this loop runs twice per character read: the first time with
    ; ah = 08h (read console, no echo syscall), the second time with
    ; ah = 02h (write console); a xor is used to switch from one
    ; mode to the other
    int 21h         ; perform syscall
    xor ah,0ah      ; switch syscall 08h <=> 02h
    cmp al,0dh      ; check if we read a newline (if we wrote stuff
                    ; we are just checking the last value read, so
                    ; no harm done; at the first iteration al starts
                    ; at 0, so no risk here)
    jnz lop         ; loop if it wasn't a newline
quit:
    ret             ; quit
