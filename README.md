# README #

Repo for most of my submissions to [codegolf.se](http://codegolf.stackexchange.com/), plus potentially some random x86 assembly experiment.

Normally everything builds with `nasm -o filename.com filename.asm` (and can be run in DosBox), although more complicated builds should have their `Makefile`.