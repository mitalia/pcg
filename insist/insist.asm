    org 100h

section .text

    ; notice: on virtually every DOS bx starts at 0 at program start
    ; see http://www.fysnet.net/yourhelp.htm
start:
    inc bx      ; bx counts the number of read characters; increment it
    mov ah,1
    int 21h     ; read a character into al
    cmp al,0dh  ; check if it's a newline
    jne start   ; repeat if it's not
    dec bx      ; we got a newline, decrement the counter
    jz start    ; if the counter became 0, it means that we only read the
                ; newline; restart (with bx being 0, as on startup)
    ret         ; otherwise quit
