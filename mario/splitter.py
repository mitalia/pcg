import PIL.Image
import numpy as np
import sys

img=PIL.Image.open(sys.argv[1])
arr=np.array(img)
out=np.zeros((arr.shape[0]/16, arr.shape[1]/16), np.uint8)
d={}
tiles=[]
for i in range(arr.shape[0]/16):
    for j in range(arr.shape[1]/16):
        tile=tuple(arr[i*16:(i+1)*16,j*16:(j+1)*16].reshape((256)))
        idx=len(tiles)
        try:
            idx=tiles.index(tile)
            d[idx]+=1
        except ValueError:
            tiles.append(tile)
            d[len(tiles)-1]=1
        out[i,j]=idx
print out
print d
