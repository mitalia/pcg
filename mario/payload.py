#!/usr/bin/env python
import sys
import PIL.Image
import numpy as np

class SpanCompactor:
    def __init__(self):
        self.out=""
        self.odd=False
        self.work=0

    def push(self, num):
        if num>15:
            self.push(0)
            self.push(num-16)
        else:
            if self.odd:
                self.work=num<<4
            else:
                self.work|=num
                self.out+=chr(self.work)
            self.odd=not self.odd

w=16
h=12

img=PIL.Image.open(sys.argv[1])
arr=np.zeros((w, h), dtype=np.uint8)
imgarr=np.array(img)
arr[0:imgarr.shape[0],0:imgarr.shape[1]] = np.array(img)
arr=arr.reshape(-1)
bitplanes=[(arr & 1<<i)>>i for i in range(2)]
scanlines=[]
for bp in bitplanes:
    b=[]
    last=0
    count=0
    for i in bp:
        if i==last:
            count+=1
        else:
            b.append(count)
            count=1
    b.append(count)
    scanlines.append(b)
out=[]
for scan in scanlines:
    sc = SpanCompactor()
    for s in scan:
        sc.push(s)
    out.append(sc.out)
    print len(sc.out)

